use Organisationfestival;

-- Calculer le prix total du festival




-- VUES

-- 1. vue pour afficher la liste du staff et leurs secteur préférentiel
DROP VIEW IF EXISTS staff_view;
CREATE VIEW staff_view
AS
	SELECT  P.No_personne,
			P.nom,
			P.prenom,
            P.No_telephone,
            P.email,
            S.Secteur_preferentiel
	FROM Staff AS S
		INNER JOIN Personne AS P
			ON S.No_personne = P.No_personne;
-- utilisation
SELECT * FROM staff_view;
            
-- 2. vue pour afficher la liste des comités et le secteur dont ils sont responsable
DROP VIEW IF EXISTS comite_view;
CREATE VIEW comite_view
AS
	SELECT  P.No_personne,
			P.nom,
			P.prenom,
            P.No_telephone,
            P.email,
            C.Secteur_responsable
	FROM Comite AS C
		INNER JOIN Personne AS P
			ON C.No_personne = P.No_personne;
-- utilisation
SELECT * FROM comite_view;
 
 
-- 3. Vue pour obtenir les informations principals des standes
DROP VIEW IF EXISTS stand_fest;
CREATE VIEW stand_fest (Nom_festival, Nom_stand, No_stand)
AS
	SELECT Nom_festival,
		   stand.Nom AS Nom_stand,
           stand.No_stand
    FROM stand
		INNER JOIN fest_acceuille_stand
        ON stand.No_Stand = fest_acceuille_stand.No_Stand;

SELECT * FROM stand_fest;

-- 4. Liste des festivals et de leurs artistes
DROP VIEW IF EXISTS artistes_fest;
CREATE VIEW artistes_fest(Nom_festival,Nom_Scene,Nom_artiste,No_artiste)
AS
	SELECT Nom_festival,
		   Artiste.Scene AS Nom_Scene,
		   artiste.Nom AS Nom_artiste,
           No_artiste
	FROM Artiste
		INNER JOIN fest_contient_scene
			ON fest_contient_scene.Nom_scene = Artiste.Scene;
            
SELECT * FROM artistes_fest;

-- 5. Liste des taches qui n'ont pas encore été validées
DROP VIEW IF EXISTS tache_a_faire;
CREATE VIEW tache_a_faire(Nom_Festival,Prenom, Nom, Description, Deadline)
AS
	SELECT emploie.Nom_festival,
		   Personne.Prenom,
		   Personne.Nom,
           charge.Description_charge,
           charge.Deadline	
    FROM Charge
		INNER JOIN charge_comite
			ON charge.No_charge = charge_comite.No_charge
		INNER JOIN comite
            ON comite.No_personne = charge_comite.No_Comite
		INNER JOIN Personne
			ON Personne.No_personne = comite.No_personne
		INNER JOIN emploie
			ON emploie.No_personne = comite.No_personne
	WHERE charge.Validation = false;

SELECT * FROM tache_a_faire;

-- TRIGGERS

-- FUNCTIONS-PROCEDURE
DROP FUNCTION IF EXISTS Prix_fest;
DELIMITER $$
CREATE FUNCTION Prix_fest (fest_name varchar(50))
RETURNS decimal (10,2)
BEGIN
	DECLARE PrixArtiste decimal(10,2);
    DECLARE PrixMatFest decimal(10,2);
    DECLARE PrixMatScene decimal(10,2);
    DECLARE PrixMatStand decimal(10,2);
    
    SELECT SUM(Artiste.Cachet) INTO PrixArtiste
    FROM artistes_fest
		INNER JOIN artiste
			ON artiste.No_artiste = artistes_fest.No_artiste
    WHERE artistes_fest.Nom_festival = fest_name;
    
    SELECT SUM(fest_besoin_mat.Prix) INTO PrixMatFest
    FROM fest_besoin_mat
    WHERE fest_besoin_mat.Nom_festival = fest_name;
    
    SELECT SUM(scene_besoin_mat.Prix) INTO PrixMatScene
    FROM fest_contient_scene
		INNER JOIN scene_besoin_mat
			ON scene_besoin_mat.Nom_scene = fest_contient_scene.Nom_scene
    WHERE fest_contient_scene.Nom_festival = fest_name;
    
    SELECT SUM(stand_besoin_mat.Prix) INTO PrixMatStand
    FROM stand_fest
		INNER JOIN stand_besoin_mat 
			ON stand_fest.No_Stand = stand_besoin_mat.No_Stand
    WHERE stand_fest.Nom_festival = fest_name;


RETURN PrixArtiste + PrixMatFest + PrixMatScene + PrixMatStand;
END $$
DELIMITER ;

SELECT Prix_fest ("La Machine à Laver 2");