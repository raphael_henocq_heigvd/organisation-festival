use Organisationfestival;
SET foreign_key_checks=0;
-- artiste
INSERT INTO artiste (Nom, Cachet,Contrat,Demandes_spécifiques,Fiche_Technique,Loge,Scene,Heure_debut,Heure_fin,Description)
	VALUES ("Heretik",500.0,"C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\ContratArtiste","Jeux de scène pyrotechnique", "C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\BesoinGrp", "Grande Salle avec les autres artistes", "TheDarkRoom",'2016-07-02 20:00:00','2016-07-03 00:00:00', "Vous allez taper du pied !"),
		   ("Dark Whisper",500.0,"C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\ContratArtiste","Jeux de Lumières très 'Dark'", "C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\BesoinGrp", "Grande Salle avec les autres artistes", "TheDarkRoom",'2016-07-03 00:00:00','2016-07-03 04:00:00', "A Trip To Darkness"),
           ("Klitorix",300.0,"C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\ContratArtiste","Vjing orgasmique", "C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\BesoinGrp", "Grande Salle avec les autres artistes", "TheDarkRoom",'2016-07-03 04:00:00','2016-07-03 06:00:00', "Live Sanglant"),
           ("Igorrr",800.0,"C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\ContratArtiste","Beacuoup d'eau sur scène pour les chanteurs", "C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\BesoinGrp", "Grande Salle avec les autres artistes", "TheDarkRoom",'2016-07-03 06:00:00','2016-07-03 08:00:00', "Accouphène Garantie !"),
           ("SP23",100.0,"C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\ContratArtiste","Apporte leur propre déco", "C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\BesoinGrp", "Grande Salle avec les autres artistes", "TheDarkRoom",'2016-07-02 08:00:00','2016-07-02 12:00:00', "Return of the French Tribe"),
           ("UZ",1000.0,"C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\ContratArtiste","Fond avec logo UZ", "C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\BesoinGrp", "Loge 1 du TRVP Dome", "TheTRVPDome",'2016-07-03 02:00:00','2016-07-02 04:00:00', "Let the masked Trap lord feed your soul with his abyssal basslines and slapping Hi-Hats"),
           ("Hucci",700.0,"C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\ContratArtiste","Fond avec logo Hucci", "C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\BesoinGrp", "Loge 2 du TRVP Dome", "TheTRVPDome",'2016-07-03 00:00:00','2016-07-02 02:00:00', "Atmospheric trap from Brighton, UK"),
           ("MrCarmack",500.0,"C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\ContratArtiste","Fond avec logo Carmack", "C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\BesoinGrp", "Loge 3 du TRVP Dome", "TheTRVPDome",'2016-07-02 22:00:00','2016-07-03 00:00:00', "Bass music pioneer to delight your hears"),
           ("FLAGZ",200.0,"C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\ContratArtiste","Fond avec logo FLAGZ", "C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\BesoinGrp", "Loge 4 du TRVP Dome", "TheTRVPDome",'2016-07-02 20:00:00','2016-07-02 22:00:00', "Trap newcomer with a deep sound");
-- assigne_tache
INSERT INTO assigne_tache VALUES(4,1,1), (1,2,2), (1,3,3), (4,4,4);

-- bar
INSERT INTO bar VALUES (1,"C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\Bar","C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\BarStocks");

-- billeterie
INSERT INTO billeterie VALUES ("Tarif Unique",0,"La Machine à Laver 2");

-- carte
INSERT INTO carte (Description, Lien, Nom_festival)
	VALUES ("Plan d'accès","C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\Map" , "La Machine à Laver 2");
    
-- charge
INSERT  INTO Charge	(Description_charge,Deadline, Validation)
	VALUES	("Planification hébergement artistes TheDarkRoom",'2016-05-01', FALSE),
			("Planification transport artistes TheDarkRoom",'2016-05-01', FALSE),
			("Planification hébergement artistes TheTRVPDome",'2016-06-01', FALSE),
			("Planification transport artistes TheTRVPDome",'2016-06-06', FALSE),
            ("Finalisation Budget",'2016-01-06', FALSE),
            ("Notification à la police du commerce",'2016-04-01', FALSE);
-- charge_comite
INSERT INTO Charge_comite
    VALUES (1,1), (2,1), (3,2), (4,2), (5,3), (6,4);
-- comite
INSERT INTO comite VALUES (1, "Resp Logistique + Scène 1"), (2, "Resp Artiste + Scène 2 "), (4, "Resp Stands"),(3, "Resp Visuel + Promo"); 

-- disponibilité 

INSERT  INTO  Disponibilité (Date_début, Date_fin, No_personne)
	VALUES	('2016-05-01 09:00:00','2016-05-20 18:00:00', 1),
			('2016-06-15 12:00:00','2016-07-10 12:00:00', 1),
            ('2016-05-15 09:00:00','2016-06-01 18:00:00', 2),
            ('2016-06-20 12:00:00','2016-07-15 12:00:00', 2);
					
-- doit_se_rendre

INSERT  INTO  doit_se_rendre (No_rendez_vous, No_personne)
	VALUES	(1, 1),(2, 1),(3, 1),(1, 2),(2, 2),(3, 2),(2, 3),(2, 4),(3, 3),(3, 4),(3, 5),(3, 6),(3, 7);

-- emploie
INSERT  INTO  emploie (Nom_festival, No_personne)
	VALUES	("La Machine à Laver 2", 1), ("La Machine à Laver 2", 2), ("La Machine à Laver 2", 3), ("La Machine à Laver 2", 4),
			("La Machine à Laver 2", 5), ("La Machine à Laver 2", 6), ("La Machine à Laver 2", 7);


-- fest_acceuille_stand
INSERT  INTO  fest_acceuille_stand (Nom_festival, No_stand)
	VALUES	("La Machine à Laver 2", 1),
			("La Machine à Laver 2", 2),
            ("La Machine à Laver 2", 3);

-- fest_besoin_mat
INSERT INTO fest_besoin_mat(Nom_festival, No_materiel,Description, Quantité, Prix)
	VALUES 	("La Machine à Laver 2", 4,"Tables pour le public", 70, 300),
			("La Machine à laver 2", 6, "Délimiter les zones du festival", 120, 500);
            
-- fest_contient_scene
INSERT INTO fest_contient_scene (Nom_festival, Nom_scene)
	VALUES 	("La Machine à Laver 2","TheDarkRoom"),
			("La Machine à Laver 2","TheTRVPDome");

-- festival
INSERT  INTO Festival VALUES("La Machine à Laver 2",'2016-07-01 20:00:00','2016-07-02 12:00:00', 7000);

-- materiel
INSERT INTO Materiel (Type_Materiel, Quantité, Contact_Responsable,Prix)   -- Mettre a jour le prix et les quantité à l'aide d'un trigger en faisant une insertion 
	VALUES	("Régie Sono", 1, "p.jeanneret@audiosound.ch", 250),
			("Ampli ", 2, "p.jeanneret@audiosound.ch", 300),
            ("Enceinte ", 4, "p.jeanneret@audiosound.ch", 150),
            ("Tables", 75, "a.billot@tableLoc.ch", 300),
            ("Tonnelles", 6, "b.épcatopn@gmail.com", 500 ),
            ("Barrières", 120, "a.bépierre@infrastructure.com", 500);

-- nourriture
INSERT INTO nourriture(No_Stand, Carte_Nourriture) VALUE (2,"C:\Users\Raphael\Documents\Cours HEIG-VD\Semestre 3\BDR\Projet\Nourriture");

-- personne
INSERT INTO Personne (Nom, Prenom, No_telephone, email)
	VALUES ("Henocq", "Raphael", "077 400 90 30", "raphael.henocq@heig-vd.ch"),("Serneels", "Guillaume", "077 100 10 10", "guillaume.serneels@heig-vd.ch"),
		   ("Fragnières", "Xavier", "077 200 20 20", "x.fragnière@gmail.com"),("GrandJean", "Luc","077 300 30 30", "l.grandjean@hmail.com"),
           ("Ge", "Emil", "077 400 40 40", "e.ge@gmail.com"),("Gschwind", "Jean", "077 500 50 50", "j.gschwind@gmail.com"),
           ("Favre","Leo", "077 600 60 60", "l.favre@gmail.com"), ("Roche", "Thomas","077 700 70 70", "t.roche@gmail.com") ;
	
-- rendez-vous
INSERT  INTO Rendez_vous ( Horaire, Lieu)
	VALUES	('2016-03-15 09:00:00','Police du commerce de Lausanne'),
			('2016-03-12 14:00:00','Assemblée générale du comité d\'organisation'),
            ('2016-05-15 09:00:00','Réunion du staff');
-- scene
INSERT INTO Scene(Nom, Localisation, Horaire_debut, Horaire_fin, Date_Montage, Date_Demontage)
	VALUES 	("TheDarkRoom", "Sud-Est du terrain",'2016-07-03 00:00:00','2016-07-03 08:00:00','2016-07-01 10:00:00','2016-07-03 14:00:00'),
			("TheTRVPDome", "Nord-Ouest du terrain",'2016-07-02 20:00:00','2016-07-03 04:00:00','2016-07-01 10:00:00','2016-07-03 14:00:00');
            
-- scene_besoin_mat
INSERT INTO scene_besoin_mat (Nom_scene, No_materiel,Description, Quantité, Prix)
	VALUES ("TheDarkRoom",1,"Table de mixage et cablage de la régie de TheDarkRoom",1, 300),
           ("TheDarkRoom",2,"Amplificateur pour la sonoristaion de TheDarkRoom",1, 500),
		   ("TheDarkRoom",3,"Enceinte pour la sonoristaion de TheDarkRoom",1, 500),
           ("TheTRVPDome",1,"Table de mixage et cablage de la régie de TheTRVPDome",1, 300),
           ("TheTRVPDome",2,"Amplificateur pour la sonoristaion de TheTRVPDome",1, 500),
		   ("TheTRVPDome",3,"Enceinte pour la sonoristaion de TheTRVPDome",1, 500); 
    
-- stand
INSERT INTO Stand (Nom, Localisation, Date_Montage,Date_Demontage,Contact_Fournisseur)
	VALUES	("Pla-thu thong", "Entrée Principale",'2016-07-01 16:00:00','2016-07-03 10:00:00', "pla-thu-thong@gmail.com"),
			("Chez Mauro", "Sur la gauche de TheDarkRoom", '2016-07-01 14:00:00','2016-07-03 10:00:00', "mauro_lausanne@gmail.com"),
            ("Double R", "En face de TheTRVPDome", '2016-07-01 14:00:00','2016-07-03 10:00:00', "double_r_yverdon@gmail.com");
            
-- Staff
INSERT INTO staff
	VALUES (5,"Securité"),(6,"Rangement"),(7, "Entrée"),(8, "Bar");

-- stand_besoin_mat
INSERT INTO stand_besoin_mat
	VALUES (1,5,"Tonnelles pour stands",2,200), (2,5,"Tonnelles pour stands",2,200), (3,5,"Tonnelles pour stands",2,200);


-- tache
INSERT INTO tache(Horaire_debut, Horaire_fin, Description)
	VALUES ('2016-07-02 20:00:00', '2016-07-03 12:00:00', "Tenir le bar Pla-thu thong"),
           ('2016-07-02 20:00:00', '2016-07-03 12:00:00', "Sécurité des deux scènes"),
           ('2016-07-03 12:00:00', '2016-07-04 12:00:00', "Rangement du site"),
           ('2016-07-02 20:00:00', '2016-07-03 12:00:00', "S'occuper de l'arrivé des spéctateurs");
           
commit;
SET foreign_key_checks=1;