DROP SCHEMA IF EXISTS OrganisationFestival;
CREATE SCHEMA OrganisationFestival;
USE OrganisationFestival;

DROP TABLE IF EXISTS Festival;
CREATE TABLE Festival (
	Nom varchar(50) NOT NULL,
    Date_debut datetime,
    Date_fin datetime,
    Budget_global decimal,
    PRIMARY KEY (Nom)
);

DROP TABLE IF EXISTS Billeterie;
CREATE TABLE Billeterie (
	Type_de_billet varchar (20) NOT NULL,
    Tarif decimal,
    Nom_festival varchar (200),

    PRIMARY KEY (Type_de_billet),
    CONSTRAINT fk_billeterie_festival FOREIGN KEY(Nom_Festival) REFERENCES Festival (Nom)
    
);   

DROP TABLE IF EXISTS Carte;
CREATE TABLE Carte (
	Num_carte int NOT NULL AUTO_INCREMENT,
    Description varchar(200),
    Lien varchar(100),
    Nom_Festival varchar(50) NOT NULL,
    PRIMARY KEY (Num_carte),
    CONSTRAINT fk_carte_festival FOREIGN KEY(Nom_Festival) REFERENCES Festival (Nom)
);

DROP TABLE IF EXISTS Personne;
CREATE TABLE Personne (
	No_personne INT NOT NULL AUTO_INCREMENT,
    Nom varchar (15) NOT NULL,
    Prenom varchar (15) NOT NULL,
    No_telephone varchar (16),
    Email varchar (30) NOT NULL,
    PRIMARY KEY (No_personne)
);

DROP TABLE IF EXISTS emploie;
CREATE TABLE emploie (
	Nom_festival varchar(50) NOT NULL,
    No_personne int NOT NULL,
    PRIMARY KEY (Nom_festival, No_personne),
    CONSTRAINT fk_emploie_festival FOREIGN KEY (Nom_festival) REFERENCES Festival (Nom),
    CONSTRAINT fk_emploie_personne FOREIGN KEY (No_personne) REFERENCES Personne (No_personne)
);

DROP TABLE IF EXISTS Rendez_vous;
CREATE TABLE Rendez_vous (
	No_rendez_vous int NOT NULL AUTO_INCREMENT,
    Horaire Datetime NOT NULL,
    Lieu varchar (50) NOT NULL,
    PRIMARY KEY (No_rendez_vous)
);

DROP TABLE IF EXISTS doit_se_rendre;
CREATE TABLE doit_se_rendre (
	No_rendez_vous int NOT NULL,
    No_personne int NOT NULL,
    PRIMARY KEY (No_rendez_vous, No_personne),
    CONSTRAINT fk_doit_se_rendre_rdv FOREIGN KEY (No_rendez_vous) REFERENCES Rendez_vous (No_rendez_vous),
    CONSTRAINT fk_doit_se_rendre_personne FOREIGN KEY (No_personne) REFERENCES Personne (No_personne)
);

DROP TABLE IF EXISTS Disponibilité;
CREATE TABLE Disponibilité (
	No_dispo int NOT NULL AUTO_INCREMENT,
    Date_début Datetime NOT NULL,
	Date_fin Datetime NOT NULL,
    No_personne int NOT NULL,
    PRIMARY KEY (No_dispo),
    CONSTRAINT fk_dispo_personne FOREIGN KEY (No_personne) REFERENCES Personne (No_personne)
);



DROP TABLE IF EXISTS Staff;
CREATE TABLE Staff (
	No_personne int NOT NULL,
    Secteur_preferentiel varchar(50),
    PRIMARY KEY (No_personne),
	CONSTRAINT fk_Staff_personne FOREIGN KEY(No_personne) REFERENCES Personne (No_personne)
);

DROP TABLE IF EXISTS Comite;
CREATE TABLE Comite (
	No_personne int NOT NULL,
    Secteur_responsable varchar(50),
    PRIMARY KEY (No_personne),
     CONSTRAINT fk_Comite_personne FOREIGN KEY(No_personne) REFERENCES Personne (No_personne)
);

DROP TABLE IF EXISTS Materiel;
CREATE TABLE Materiel (
	No_Materiel int NOT NULL AUTO_INCREMENT,
    Type_Materiel varchar(50),
    Quantité int NOT NULL, -- Calculé à partir des quantité des tables besoin_mat
    Contact_Responsable varchar (100),
    Prix decimal,
    PRIMARY KEY (No_Materiel)
);

DROP TABLE IF EXISTS fest_besoin_mat;
CREATE TABLE fest_besoin_mat (
	Nom_festival varchar(50) NOT NULL,
	No_materiel int NOT NULL,
	Description varchar(500),
    Quantité int NOT NULL,
    Prix decimal,
    PRIMARY KEY (Nom_festival, No_materiel),
    CONSTRAINT fk_besoin_festival FOREIGN KEY (Nom_festival) REFERENCES Festival (Nom),
    CONSTRAINT fk_besoin_mat FOREIGN KEY (No_materiel) REFERENCES Materiel (No_materiel)
);


DROP TABLE IF EXISTS Stand;
CREATE TABLE Stand (
	No_Stand int NOT NULL AUTO_INCREMENT,
    Nom varchar(50) NOT NULL,
    Localisation varchar(50) NOT NULL,
    Date_Montage datetime NOT NULL,
    Date_Demontage datetime NOT NULL,
	Contact_Fournisseur varchar(100),
    PRIMARY KEY (No_Stand)
);

DROP TABLE IF EXISTS stand_besoin_mat;
CREATE TABLE stand_besoin_mat (
	No_Stand int NOT NULL,
	No_materiel int NOT NULL,
	Description varchar(500),
    Quantité int NOT NULL,
    Prix decimal,
    PRIMARY KEY (No_Stand, No_materiel),
    CONSTRAINT fk_stand_besoin_mat FOREIGN KEY (No_Stand) REFERENCES Stand (No_Stand),
    CONSTRAINT fk_mat_besoin_stand FOREIGN KEY (No_materiel) REFERENCES Materiel (No_Materiel)
);

DROP TABLE IF EXISTS fest_acceuille_stand;
CREATE TABLE fest_acceuille_stand (
	Nom_festival varchar(50) NOT NULL,
	No_stand int NOT NULL,
    PRIMARY KEY (Nom_festival, No_stand),
    CONSTRAINT fk_acceuille_festival FOREIGN KEY (Nom_festival) REFERENCES Festival (Nom),
    CONSTRAINT fk_acceuille_stand FOREIGN KEY (No_stand) REFERENCES Stand (No_stand)
);

DROP TABLE IF EXISTS Scene;
CREATE TABLE Scene (

	Nom varchar(50) NOT NULL,
    Localisation varchar(100) NOT NULL,
    Horaire_debut datetime NOT NULL,
    Horaire_fin datetime NOT NULL,
    Date_Montage datetime NOT NULL,
    Date_Demontage datetime NOT NULL,
    PRIMARY KEY (Nom)
);

DROP TABLE IF EXISTS scene_besoin_mat;
CREATE TABLE scene_besoin_mat (
	Nom_scene varchar(50) NOT NULL,
	No_materiel int NOT NULL,
	Description varchar(500),
    Quantité int NOT NULL,
    Prix decimal,
    PRIMARY KEY (Nom_scene, No_materiel),
    CONSTRAINT fk_scene_besoin_mat FOREIGN KEY (Nom_scene) REFERENCES Scene (Nom),
    CONSTRAINT fk_mat_besoin_scene FOREIGN KEY (No_materiel) REFERENCES Materiel (No_Materiel)
);


DROP TABLE IF EXISTS Artiste;
CREATE TABLE Artiste (

	No_artiste int NOT NULL AUTO_INCREMENT,
    Nom varchar(100) NOT NULL,
    Cachet double,
    Contrat varchar(100),  -- Lien vers le pdf du contrat
    Demandes_spécifiques varchar(500),
    Fiche_Technique varchar(100), -- Lien vers un pdf
    Loge varchar(500),
    Scene varchar(50),
    Heure_debut datetime NOT NULL,
    Heure_fin datetime NOT NULL,
    Description varchar(500),
    PRIMARY KEY(No_artiste),
    CONSTRAINT fk_Artiste_scene FOREIGN KEY(Scene) REFERENCES Scene (Nom)

);

DROP TABLE IF EXISTS fest_contient_scene;
CREATE TABLE fest_contient_scene (
	Nom_festival varchar(50) NOT NULL,
	Nom_scene varchar(50) NOT NULL,
    PRIMARY KEY (Nom_festival, Nom_scene),
    CONSTRAINT fk_contient_festival FOREIGN KEY (Nom_festival) REFERENCES Festival (Nom),
    CONSTRAINT fk_contient_scene FOREIGN KEY (Nom_scene) REFERENCES Scene (Nom)
);

DROP TABLE IF EXISTS Charge;
CREATE TABLE Charge (
	
    No_Charge int NOT NULL AUTO_INCREMENT,
    Description_charge varchar (500),
    Deadline date,
    Validation bool DEFAULT false,
    PRIMARY KEY (No_Charge)
);

DROP TABLE IF EXISTS Charge_comite;
CREATE TABLE Charge_comite (
	 No_Charge int NOT NULL,
	 No_Comite int NOT NULL,
     PRIMARY KEY (No_Charge, No_Comite),
     CONSTRAINT fk_Charge_comite_charge FOREIGN KEY (No_Charge) REFERENCES Charge (No_Charge),
     CONSTRAINT fk_Charge_comite_comité FOREIGN KEY (No_Comite) REFERENCES Comite (No_Personne)
     
);

DROP TABLE IF EXISTS Nourriture;
CREATE TABLE Nourriture (
	No_Stand int NOT NULL,
    Carte_Nourriture varchar(100), -- Lien vers un fichier
    PRIMARY KEY (No_Stand),
    CONSTRAINT fk_Nourriture_Stand FOREIGN KEY(No_Stand) REFERENCES Stand (No_Stand)
);

DROP TABLE IF EXISTS Bar;
CREATE TABLE Bar (
	No_Stand int NOT NULL,
    Carte_Boisson varchar(100), -- Lien vers un fichier
    Stocks varchar(100), -- Lien vers un fichier
    PRIMARY KEY (No_Stand),
    CONSTRAINT fk_Bar_Stand FOREIGN KEY(No_Stand) REFERENCES Stand (No_Stand)

);

DROP TABLE IF EXISTS Tache;
CREATE TABLE Tache (
	No_Tache int NOT NULL AUTO_INCREMENT,
    Horaire_debut datetime NOT NULL,
    Horaire_fin datetime NOT NULL,
    Description varchar(250),
    PRIMARY KEY (No_Tache)
);

DROP TABLE IF EXISTS Assigne_tache;
CREATE TABLE Assigne_tache (
	No_Comite int NOT NULL,
    No_Staff int NOT NULL,
    No_Tache int NOT NULL,
    PRIMARY KEY (No_Comite, No_Staff, No_Tache),
	CONSTRAINT fk_assigne_comite FOREIGN KEY(No_Comite) REFERENCES Comite (No_Personne),
	CONSTRAINT fk_assigne_staff FOREIGN KEY(No_Staff) REFERENCES Staff (No_Personne),
	CONSTRAINT fk_assigne_tache FOREIGN KEY(No_Tache) REFERENCES Tache (No_Tache)
    
);
