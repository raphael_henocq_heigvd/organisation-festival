============ Instruction de d�ploiement de la base de donn�e OrganisationFestival ============ 

1) Ex�cuter le fichier CreationBDR.sql afin de g�n�rer la base de donn�e
2) Ex�cuter le fichier Trigger_view_event.sql afin d'ajouter ces fonctions � la base de donn�e
3) (optionnel) Ex�cuter le fichier data.sql pour inserer des donn�es pr�d�finies dans la base de donn�e
4) Modifier la fonction init() dans le fichier SQLserver.java pour ajouter votre username et votre mot de passe
5) D�marer l'application java

============ Cr�ation d'un festival avec la base de donn�e ============ 
1) Lancez l'application
2) Commencez par ajouter un festival en entrant les champs demand�.
3) Vous pouvez ensuite naviguer dans les diff�rentes cat�gories 
	A)Comit�     : Afficher les membres faisant partie du comit�. Permet aussi d'y ajouter des membres
	B)Staff      : Afficher les membres faisant partie du Staff. Permet d'y ajouter d'autres personnes
	C)Sc�ne      : Affiche les diff�rentes Sc�nes du festival. Permet aussi d'en ajouter
		     : Double-cliquez sur une sc�ne pour afficher les artistes qui s'y produiront. Permet aussi d'ajouter d'autres Artistes ainsi que le mat�riel n�cessaire � cette sc�ne
	D)Stand      : Affiche tout les stands du festival. Permet d'ajouter diff�rent types de stands
		     : Double-cliquez sur un stand pour afficher ces informations d�taill�s et ajouter le mat�riel dont il a besoin
	E)Billeterie : Affiche les diff�rents billets disponible pour l'�v�nement, permet aussi d'en ajouter
	F)Mat�riel   : Affiche tout le mat�riel dont a besoin le festival. Permet aussi d'ajouter le mat�riel n�cessaire au festival (diff�rent du mat�riel de sc�ne et de stand)
	G)Carte	     : Affiche les diff�rentes carte du festival