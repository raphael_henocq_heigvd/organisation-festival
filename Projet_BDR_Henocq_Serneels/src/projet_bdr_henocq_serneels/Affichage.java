package projet_bdr_henocq_serneels;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import static java.awt.BorderLayout.CENTER;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;



public class Affichage extends JFrame{
      private SQLserver sql = new SQLserver();

   public Affichage() {
      try {
    	  
    	  sql.init();
    	  
      } catch (SQLException e) {
    	  
         System.out.println("Erreur initialisation " + e.getMessage());
         
      }
      
      panel = new JPanel[9];
      buttonA = new JButton[8];
      buttonB = new JButton[9];
      frame = new JFrame[16];
      box = new JComboBox[8];
      label = new JLabel[8];
      buttonValider = new JButton[14];
      champSaisie = new JTextField[14][];
      for (int i = 0 ; i < 8 ; i++)
         panel[i] = new JPanel();
   }
   
   public void affiche()
   {
      
      setSize(dim);
      setDefaultCloseOperation(EXIT_ON_CLOSE);
      JPanel tmp[] = new JPanel[7];
      
      final Object[] choices = sql.getNameFestival();
      setLayout(new GridLayout(8,1));
      label[0] = new JLabel(" Festivals : ");
      panel[0].add(label[0]);
      box[0] = new JComboBox<>(choices);
      panel[0].add(box[0]);
      buttonA[0] = new JButton("Add");
      panel[0].add(buttonA[0]);
      add(panel[0]);
      
      for (int i = 0; i < buttonAName.length; i ++)
      {
         tmp[i]=new JPanel();
         buttonA[i+1]=new JButton(buttonAName[i]);
         buttonA[i+1].setPreferredSize(dimBouton);
         tmp[i].add(buttonA[i+1]);
         add(tmp[i]);
         
      }
      
       
      //Add festival
      buttonA[0].addActionListener((ActionEvent e) -> {
         addFestival();
      });
      
      //Affiche Comités
      buttonA[1].addActionListener((ActionEvent e) -> {
         afficheComite((String) box[0].getSelectedItem());
      });
      
      //Affiche staff
      buttonA[2].addActionListener((ActionEvent e) -> {
         afficheStaff((String) box[0].getSelectedItem());
      });
      
      //Afficher Scene
      buttonA[3].addActionListener((ActionEvent e) -> {
         afficheScene((String) box[0].getSelectedItem());
      });
      
      //Afficher Stand
      buttonA[4].addActionListener((ActionEvent e) -> {
         afficheStand((String) box[0].getSelectedItem());
      });
      
      //Afficher Billeterie
      buttonA[5].addActionListener((ActionEvent e) -> {
         afficheBilleterie((String) box[0].getSelectedItem());
      });
      
      //Afficher Matériel
      buttonA[6].addActionListener((ActionEvent e) -> {
         afficheMateriel((String) box[0].getSelectedItem());
      });
      
      //Afficher Carte
      buttonA[7].addActionListener((ActionEvent e) -> {
         afficheCarte((String) box[0].getSelectedItem());
      });
      
      
      setVisible(true);
   }
   
   
   // 0 : Festival 1:Comite  2:Staff  3:Scene  4:Stand 5:Billeterie 6:Materiel(du festival) 7: Carte
   final private JPanel panel[];
   //private JPanel mainPanelFest;
   final private JButton buttonA[];
   final private JButton buttonB[];
   final private JButton buttonValider[];
   private final JFrame frame[];
   final private JComboBox<Object> box[];
   final private JLabel label[];
   final private JTextField champSaisie[][];
   private final String buttonAName[] =  {"Comite" ,"Staff", "Scene", "Stand", "Billeterie", "Materiel", "Carte"};
   private final Dimension dim= new Dimension(1024,780);
   private final Dimension dimTableau= new Dimension(820,780);
   private final Dimension dimBouton= new Dimension(160,40);
   
   private void addFestival()
   {
      frame[0] = new JFrame();
      frame[0].setSize(dim);
      frame[0].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[0].setTitle("Ajout Festival");
      frame[0].setLayout(new GridLayout(5,1));
      String[] names = {"Nom", "Début (YYY/MM/JJ HH:MM:SS)", "Fin", "Budget"};
      JPanel panel[] = new JPanel[4];
      setPanelB(frame[0],panel, 10, 4, names);     
      buttonValider[10].addActionListener((ActionEvent e) -> {
         sql.insertFestival(champSaisie[10][0].getText(), champSaisie[10][1].getText(), champSaisie[10][2].getText(), Double.valueOf(champSaisie[10][3].getText()));
         frame[0].dispose();
         System.out.println("insert OK");
         removeAll();
         repaint();
         affiche();
      });
      frame[0].setVisible(true);
   }
   
   private void afficheComite(String nomFestival)
   {
      final Object[][] result = sql.getComite(nomFestival);
      final String[] entete = {"No", "Nom", "Prenom", "Telephone", "email" , "Secteur Responsable"};
      frame[1] = new JFrame();
      frame[1].setSize(dim);
      frame[1].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[1].setTitle("Comités");
      
      panel[1]=new JPanel();
      JTable tableau = new JTable (result,entete);
      tableau.setEnabled(false);
      tableau.setPreferredScrollableViewportSize(dimTableau);
      panel[1].add(new JScrollPane(tableau), CENTER);
      buttonB[1] = new JButton("Add");
      panel[1].add(buttonB[1]);      
      buttonB[1].addActionListener((ActionEvent e) -> {
         addComite(nomFestival);
      });
      
      frame[1].add(panel[1]);      
      frame[1].setVisible(true);
   }
   
   private void afficheStaff(String nomFestival)
   {
      final Object[][] result = sql.getStaff(nomFestival);
      final String[] entete = {"No", "Nom", "Prenom", "Telephone", "email" , "Secteur Préféré"};
      
      frame[2] = new JFrame();
      frame[2].setSize(dim);
      frame[2].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[2].setTitle("Staffs");
      
      panel[2]=new JPanel();
      JTable tableau = new JTable (result,entete);
      tableau.setEnabled(false);
      tableau.setPreferredScrollableViewportSize(dimTableau);
      panel[2].add(new JScrollPane(tableau), CENTER);
      buttonB[2] = new JButton("Add");
      panel[2].add(buttonB[2]);      
      buttonB[2].addActionListener((ActionEvent e) -> {
         addStaff(nomFestival);
      });
      frame[2].add(panel[2]);
      
      frame[2].setVisible(true);
   }
   
   private void afficheScene(String nomFestival)
   {
      final Object[][] result = sql.getScene(nomFestival);
      final String[] entete = {"Nom", "Localisation", "Debut", "Fin", "Montage" , "Demontage"};
      
      frame[3] = new JFrame();
      frame[3].setSize(dim);
      frame[3].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[3].setTitle("Scènes");
      
      panel[3]=new JPanel();
      JTable tableau = new JTable (result,entete);
      tableau.setRowSelectionAllowed(true);
      tableau.setPreferredScrollableViewportSize(dimTableau);
      tableau.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
         public void valueChanged(ListSelectionEvent event) {
            if (event.getValueIsAdjusting()) {
                return;
            }
            int row = tableau.getSelectedRow();
            String nomScene = tableau.getValueAt(row, 0).toString();
            System.out.println(nomScene);
            afficheArtiste(nomFestival,nomScene);
        }
      });
      panel[3].add(new JScrollPane(tableau), CENTER);
      buttonB[3] = new JButton("Add");
      panel[3].add(buttonB[3]);      
      buttonB[3].addActionListener((ActionEvent e) -> {
         addScene(nomFestival);
      });
      frame[3].add(panel[3]);
      
      frame[3].setVisible(true);
   }
   
  private void afficheStand(String nomFestival)
   {
      final Object[][] result = sql.getStand(nomFestival);
      final String[] entete = {"No_Satnd","Nom", "Localisation", "Montage" , "Demontage"};
      JButton buttonAdd1 = new JButton("Add Stand Nourriture");
      buttonAdd1.setPreferredSize(dimBouton);
      JButton buttonAdd2 = new JButton("Add Stand Bar+Nourriture");
      buttonAdd2.setPreferredSize(dimBouton);
      JPanel tmp1 = new JPanel();
      JPanel tmp2 = new JPanel();
      JPanel tmp3 = new JPanel();
      
      
      frame[4] = new JFrame();
      frame[4].setSize(dim);
      frame[4].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[4].setTitle("Stands");
      frame[4].setLayout(new GridLayout(2,1));
      
      panel[4]=new JPanel(new GridLayout(1,3));
      JTable tableau = new JTable (result,entete);
      tableau.setRowSelectionAllowed(true);
      tableau.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
         public void valueChanged(ListSelectionEvent event) {
            if (event.getValueIsAdjusting()) {
                return;
            }
            int row = tableau.getSelectedRow();
            System.out.println (row);
            int noStand = Integer.valueOf(tableau.getValueAt(row, 0).toString());
            System.out.println (noStand);
            afficheStand(nomFestival,noStand);
        }
      });
      frame[4].add(new JScrollPane(tableau), CENTER);
      buttonB[4] = new JButton("Add Bar");
      buttonB[4].setPreferredSize(dimBouton);
      tmp1.add(buttonB[4]);
      panel[4].add(tmp1); 
      tmp2.add(buttonAdd1);
      tmp3.add(buttonAdd2);
      panel[4].add(tmp2);
      panel[4].add(tmp3);
      buttonB[4].addActionListener((ActionEvent e) -> {
         addBar(nomFestival);
      });
      buttonAdd1.addActionListener((ActionEvent e) -> {
         addStandNourriture(nomFestival);
      });
      buttonAdd2.addActionListener((ActionEvent e) -> {
         addStand(nomFestival);
      });
      
      frame[4].add(panel[4]);
      
      frame[4].setVisible(true);
   }
  
   private void afficheStand(String nomFestival, int noStand)
   {
      JFrame jframe = new JFrame();
      jframe.setLayout(new GridLayout(2,1));
      JPanel jpanel = new JPanel();
      String[] entete;
      final Object[][] result = sql.getStand(nomFestival,noStand);
      String tmp[] = {"No_Satnd","Nom", "Localisation", "Montage" , "Demontage"};
      if (result[0].length == 5)    //Stand Nourriture
      {
         entete = new String[5];   
         for (int i = 0; i < tmp.length; i++)
            entete[i]=tmp[i];
      }
      else if (result[0].length == 6)    //Stand Nourriture
      {
         entete = new String[6];   
         for (int i = 0; i < tmp.length; i++)
            entete[i]=tmp[i];
         entete[5] = "Carte Menu";
      } else if (result[0].length == 7)  //Stand Boisson
      {
         entete = new String[7];
         for (int i = 0; i < tmp.length; i++)
            entete[i]=tmp[i];
         entete[5] = "Carte Boisson";
         entete[6] = "Stock";
      }
      else
      {
         entete = new String[8];
         for (int i = 0; i < tmp.length; i++)
            entete[i]=tmp[i];
         entete[5] = "Carte Boisson";
         entete[6] = "Stock";
         entete[7] = "Carte Menu";
      }
      JButton buttonAdd1 = new JButton("Add Materiel");

      jframe.setSize(dim);
      jframe.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      jframe.setTitle("Stand");
      
      JTable tableau = new JTable (result,entete);
      tableau.setRowSelectionAllowed(true);
      
      jframe.add(new JScrollPane(tableau), CENTER);
      buttonAdd1.setPreferredSize(dimBouton);
      jpanel.add(buttonAdd1);
      
      buttonAdd1.addActionListener((ActionEvent e) -> {
         addMaterielStand(nomFestival,noStand);
      });
            
      jframe.add(jpanel);
      
      jframe.setVisible(true);
   }
  
  private void afficheBilleterie(String nomFestival)
   {
      final Object[][] result = sql.getBilleterie(nomFestival);
      final String[] entete = {"Billet", "Tarif"};
      frame[5] = new JFrame();
      frame[5].setSize(dim);
      frame[5].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[5].setTitle("Billeterie");
      
      panel[5]=new JPanel();
      JTable tableau = new JTable (result,entete);
      tableau.setEnabled(false);
      tableau.setPreferredScrollableViewportSize(dimTableau);
      panel[5].add(new JScrollPane(tableau), CENTER);
      buttonB[5] = new JButton("Add");
      panel[5].add(buttonB[5]);      
      buttonB[5].addActionListener((ActionEvent e) -> {
         addBilleterie(nomFestival);
      });
      frame[5].add(panel[5]);
      
      frame[5].setVisible(true);
   }
  
  private void afficheMateriel(String nomFestival)
   {
      final Object[][] result = sql.getMateriel(nomFestival);
      final String[] entete = {"No_materiel", "Type_Materiel", "Quantité", "Prix"};
      frame[6] = new JFrame();
      frame[6].setSize(dim);
      frame[6].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[6].setTitle("Matériel");
      
      panel[6]=new JPanel();
      JTable tableau = new JTable (result,entete);
      tableau.setEnabled(false);
      tableau.setPreferredScrollableViewportSize(dimTableau);
      panel[6].add(new JScrollPane(tableau), CENTER);
      buttonB[6] = new JButton("Add");
      panel[6].add(buttonB[6]);      
      buttonB[6].addActionListener((ActionEvent e) -> {
         addMaterielFestival(nomFestival);
      });
      frame[6].add(panel[6]);
      
      frame[6].setVisible(true);
   }
  
  private void afficheCarte(String nomFestival)
   {
      final Object[][] result = sql.getCarte(nomFestival);
      final String[] entete = {"Num_carte", "Description", "Lien"};
      
      frame[7] = new JFrame();
      frame[7].setSize(dim);
      frame[7].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[7].setTitle("Carte");
      
      panel[7]=new JPanel();
      JTable tableau = new JTable (result,entete);
      tableau.setEnabled(false);
      tableau.setPreferredScrollableViewportSize(dimTableau);
      panel[7].add(new JScrollPane(tableau), CENTER);
      buttonB[7] = new JButton("Add");
      panel[7].add(buttonB[7]);      
      buttonB[7].addActionListener((ActionEvent e) -> {
         addCarte(nomFestival);
      });
      frame[7].add(panel[7]);
      
      frame[7].setVisible(true);
   }
  
  private void afficheArtiste(String nomFestival, String nomScene)
  {
     final Object[][] result = sql.getArtiste(nomScene);
      final String[] entete = {"Nom", "Cachet", "Demandes","Fiches_Techique","Loge","Début","Fin","Description"};
      JButton but = new JButton("Add Materiel");
      
      frame[15] = new JFrame();
      frame[15].setSize(dim);
      frame[15].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[15].setTitle("Artiste");
      frame[15].setLayout(new GridLayout(2,1));
      
      panel[8]=new JPanel();
      JTable tableau = new JTable (result,entete);
      tableau.setEnabled(false);
      tableau.setPreferredScrollableViewportSize(dimTableau);
      frame[15].add(new JScrollPane(tableau), CENTER);
      buttonB[8] = new JButton("Add Artiste"); 
 
      but.setPreferredSize(dimBouton);
      buttonB[8].setPreferredSize(dimBouton);
      panel[8].add(buttonB[8]);
      panel[8].add(but);
      frame[15].add(panel[8]);
      
      buttonB[8].addActionListener((ActionEvent e) -> {
         addArtiste(nomFestival, nomScene);
      });
      but.addActionListener((ActionEvent e) -> {
         addMaterielScene(nomFestival, nomScene);
      });
      
      frame[15].setVisible(true);
  }
  
  private void addComite(String nomFestival)
  {
      frame[8] = new JFrame();
      frame[8].setSize(dim);
      frame[8].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[8].setTitle("Ajout Comités");
      frame[8].setLayout(new GridLayout(6,1));
      String[] names = {"Nom", "Prénom", "Téléphone", "Email", "Secteur Responsable"};
      JPanel panel[] = new JPanel[5];
      setPanelB(frame[8],panel, 0, 5, names);     
      buttonValider[0].addActionListener((ActionEvent e) -> {
         sql.insertComité(nomFestival, champSaisie[0][0].getText(), champSaisie[0][1].getText(), champSaisie[0][2].getText(), champSaisie[0][3].getText(), champSaisie[0][4].getText());
         frame[8].dispose();
         frame[1].dispose();
         afficheComite(nomFestival);
      });
      
      frame[8].setVisible(true);
  }
  
  private void addStaff(String nomFestival)
  {
      frame[9] = new JFrame();
      frame[9].setSize(dim);
      frame[9].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[9].setTitle("Ajout Staff");
      frame[9].setLayout(new GridLayout(6,1));
      String[] names = {"Nom", "Prénom", "Téléphone", "Email", "Secteur Préférentiel"};
      JPanel panel[] = new JPanel[5];
      setPanelB(frame[9],panel, 1, 5, names);     
      buttonValider[1].addActionListener((ActionEvent e) -> {
         sql.insertStaff(nomFestival, champSaisie[1][0].getText(), champSaisie[1][1].getText(), champSaisie[1][2].getText(), champSaisie[1][3].getText(), champSaisie[1][4].getText());
         frame[9].dispose();
         frame[2].dispose();
         afficheStaff(nomFestival);
      });
      
      frame[9].setVisible(true);
  }
  
  private void addScene(String nomFestival)
  {
      frame[10] = new JFrame();
      frame[10].setSize(dim);
      frame[10].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[10].setTitle("Ajout Scene");
      frame[10].setLayout(new GridLayout(6,1));
      String[] names = {"Nom", "Localisation", "Debut(YYYY/MM/DD HH:MM:SS)", "Fin", "Montage","Demontage"};
      JPanel panel[] = new JPanel[6];
      setPanelB(frame[10],panel, 2, 6, names);     
      buttonValider[2].addActionListener((ActionEvent e) -> {
         sql.insertScene(nomFestival, champSaisie[2][0].getText(), champSaisie[2][1].getText(), champSaisie[2][2].getText(), champSaisie[2][3].getText(), champSaisie[2][4].getText(), champSaisie[2][5].getText());
         frame[10].dispose();
         frame[3].dispose();
         afficheScene(nomFestival);
      });
      frame[10].setVisible(true);
  }
  
  private void addBar(String nomFestival)
  {
     JFrame CurFrame = new JFrame();
      CurFrame.setSize(dim);
      CurFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      CurFrame.setTitle("Ajout Bar");
      CurFrame.setLayout(new GridLayout(8,1));
      String[] names = {"Nom", "Localisation", "Montage(YYYY/MM/DD HH:MM:SS)", "Demontage", "Fournisseur","Carte Boisson","Stock"};
      JPanel panel[] = new JPanel[7];
      setPanelB(CurFrame,panel, 3, 7, names);     
      buttonValider[3].addActionListener((ActionEvent e) -> {
         sql.insertStandBar(nomFestival, champSaisie[3][0].getText(), champSaisie[3][1].getText(), champSaisie[3][2].getText(), champSaisie[3][3].getText(), champSaisie[3][4].getText(),champSaisie[3][5].getText(),champSaisie[3][6].getText());
         CurFrame.dispose();
         frame[4].dispose();
         afficheStand(nomFestival);
      });
      CurFrame.setVisible(true);
     
  }
  
  private void addStandNourriture(String nomFestival)
  {
     JFrame CurFrame = new JFrame();
      CurFrame.setSize(dim);
      CurFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      CurFrame.setTitle("Ajout Bar");
      CurFrame.setLayout(new GridLayout(7,1));
      String[] names = {"Nom", "Localisation", "Montage(YYYY/MM/DD HH:MM:SS)", "Demontage", "Fournisseur","Carte Boisson","Stock"};
      JPanel panel[] = new JPanel[6];
      setPanelB(CurFrame,panel, 4, 6, names);     
      buttonValider[4].addActionListener((ActionEvent e) -> {
         sql.insertStandNourriture(nomFestival, champSaisie[4][0].getText(), champSaisie[4][1].getText(), champSaisie[4][2].getText(), champSaisie[4][3].getText(), champSaisie[4][4].getText(),champSaisie[4][5].getText());
         CurFrame.dispose();
         frame[4].dispose();
         afficheStand(nomFestival);
      });
      CurFrame.setVisible(true);
     
  }
  
  private void addStand(String nomFestival)
  {
      frame[11] = new JFrame();
      frame[11].setSize(dim);
      frame[11].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[11].setTitle("Ajout Stand");
      frame[11].setLayout(new GridLayout(9,1));
      String[] names = {"Nom", "Localisation", "Montage(YYYY/MM/DD HH:MM:SS)", "Demontage", "Fournisseur", "Carte Menu","Carte Boisson","Stock"};
      JPanel panel[] = new JPanel[8];
      setPanelB(frame[11],panel, 5, 8, names);     
      buttonValider[5].addActionListener((ActionEvent e) -> {
         sql.insertStandBarNourriture(nomFestival, champSaisie[5][0].getText(), champSaisie[5][1].getText(), champSaisie[5][2].getText(), champSaisie[5][3].getText(), champSaisie[5][4].getText(), champSaisie[5][5].getText(),champSaisie[5][6].getText(),champSaisie[5][7].getText());
         frame[11].dispose();
         frame[4].dispose();
         afficheStand(nomFestival);
      });
      frame[11].setVisible(true);
  }
  
  private void addBilleterie(String nomFestival)
  {
      frame[12] = new JFrame();
      frame[12].setSize(dim);
      frame[12].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[12].setTitle("Ajout Billet");
      frame[12].setLayout(new GridLayout(3,1));
      String[] names = {"Type de billet", "Tarif"};
      JPanel panel[] = new JPanel[2];
      setPanelB(frame[12],panel, 6, 2, names);     
      buttonValider[6].addActionListener((ActionEvent e) -> {
         sql.insertBilleterie(nomFestival, champSaisie[6][0].getText(), Double.valueOf(champSaisie[6][1].getText()));
         frame[12].dispose();
         frame[5].dispose();
         afficheBilleterie(nomFestival);
      });
      
      frame[12].setVisible(true);
  }
  
  private void addMaterielFestival(String nomFestival)
  {
      frame[13] = new JFrame();
      frame[13].setSize(dim);
      frame[13].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[13].setTitle("Ajout Festival Matériel");
      frame[13].setLayout(new GridLayout(3,1));
      String[] names = {"Type", "Description", "Quantité","Contact", "Prix" };
      JPanel panel[] = new JPanel[5];
      setPanelB(frame[13],panel, 7, 5, names);     
      buttonValider[7].addActionListener((ActionEvent e) -> {
         sql.insertMaterielFest(nomFestival, champSaisie[7][0].getText(),champSaisie[7][1].getText(), Integer.valueOf(champSaisie[7][2].getText()),champSaisie[7][3].getText(),Double.valueOf(champSaisie[7][4].getText()));
         frame[13].dispose();
         frame[6].dispose();
         afficheMateriel(nomFestival);
      });
      
      frame[13].setVisible(true);
  }
  
  private void addCarte(String nomFestival)
  {
      frame[14] = new JFrame();
      frame[14].setSize(dim);
      frame[14].setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      frame[14].setTitle("Ajout Carte");
      frame[14].setLayout(new GridLayout(3,1));
      String[] names = {"Description", "Lien"};
      JPanel panel[] = new JPanel[2];
      setPanelB(frame[14],panel, 8, 2, names);     
      buttonValider[8].addActionListener((ActionEvent e) -> {
         sql.insertCarte(nomFestival, champSaisie[8][0].getText(), champSaisie[8][1].getText());
         frame[14].dispose();
         frame[7].dispose();
         afficheCarte(nomFestival);
      });
      
      frame[14].setVisible(true);
  }
   
  private void addArtiste(String nomFestival, String nomScene)
  {
     JFrame jframe = new JFrame();
      jframe.setSize(dim);
      jframe.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      jframe.setTitle("Ajout Artiste");
      jframe.setLayout(new GridLayout(9,1));
      String[] names = {"Nom", "Cachet", "Contrat", "Demandes", "Fiche_Technique","Loge","Debut(YYYY/MM/JJ HH:MM:SS)", "Fin", "Description"};
      JPanel panel[] = new JPanel[9];
      setPanelB(jframe,panel, 11, 9, names);     
      buttonValider[11].addActionListener((ActionEvent e) -> {
         sql.insertArtiste(nomScene, champSaisie[11][0].getText(), Double.valueOf(champSaisie[11][1].getText()),  champSaisie[11][2].getText()
                          , champSaisie[11][3].getText(), champSaisie[11][4].getText(),  champSaisie[11][5].getText()
                          , champSaisie[11][6].getText(), champSaisie[11][7].getText(), champSaisie[11][8].getText());
         jframe.dispose();
         frame[15].dispose();
         afficheArtiste(nomFestival, nomScene);
      });
      
      jframe.setVisible(true);
  }
  
  private void addMaterielStand (String nomFestival, int noStand)
  {
     JFrame jframe = new JFrame();
      jframe.setSize(dim);
      jframe.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      jframe.setTitle("Ajout Matériel Stand");
      jframe.setLayout(new GridLayout(6,1));
      String[] names = {"Type", "Description", "Quantité","Contact", "Prix" };
      JPanel panel[] = new JPanel[5];
      setPanelB(jframe,panel, 12, 5, names);     
      buttonValider[12].addActionListener((ActionEvent e) -> {
         sql.insertMaterielStand(noStand, champSaisie[12][0].getText(),champSaisie[12][1].getText(), Integer.valueOf(champSaisie[12][2].getText()),  champSaisie[12][3].getText(),Double.valueOf(champSaisie[12][4].getText()));
         jframe.dispose();
      });
      
      jframe.setVisible(true);
     
  }
  
  private void addMaterielScene (String nomFestival, String nomScene)
  {
     JFrame jframe = new JFrame();
      jframe.setSize(dim);
      jframe.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      jframe.setTitle("Ajout Matériel Scene");
      jframe.setLayout(new GridLayout(6,1));
      String[] names = {"Type", "Description", "Quantité","Contact", "Prix" };
      JPanel panel[] = new JPanel[5];
      setPanelB(jframe,panel, 13, 5, names);     
      buttonValider[13].addActionListener((ActionEvent e) -> {
         sql.insertMaterielScene(nomScene, champSaisie[13][0].getText(),champSaisie[13][1].getText(), Integer.valueOf(champSaisie[13][2].getText()),  champSaisie[13][3].getText(),Double.valueOf(champSaisie[13][4].getText()));
         jframe.dispose();
      });
      
      jframe.setVisible(true);
     
  }
  private void setPanelB(JFrame frame,JPanel panel[], int index, int size, String... names)
  {
     champSaisie[index]=new JTextField[size];
     for (int i = 0 ; i < size ;i++)
     {
        panel[i]= new JPanel();
       // panel[i].setLayout(new BoxLayout (panel[i],BoxLayout.LINE_AXIS));
        JTextField j = new JTextField(names[i]);
        j.setEditable(false);
        panel[i].add(j);
        champSaisie[index][i]=new JTextField(20);
        panel[i].add(champSaisie[index][i]);
        frame.getContentPane().add(panel[i]);
     }
     
     buttonValider[index] = new JButton ("Valider");
      buttonValider[index].setPreferredSize(new Dimension(60,80));
     frame.getContentPane().add(buttonValider[index]);
     
     frame.setVisible(true);    
  }
}


  