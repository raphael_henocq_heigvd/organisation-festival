package projet_bdr_henocq_serneels;

import java.util.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;
import java.sql.ResultSetMetaData;


/**
 *
 * @author Raphael
 * 
 */
public class SQLserver 
{
   private static final Logger mLog = Logger.getLogger(SQLserver.class.getName()); 
   private static final Scanner mScanner = new Scanner(System.in);
   private Connection mConnection;
   
   public void init() throws SQLException {
        // Example URL, update accordingly !
        String url = "jdbc:mysql://localhost:3306/organisationfestival?user=root&password=scorpion";
        mConnection = DriverManager.getConnection(url);
    }
   
   public Object[] getNameFestival() {
      Vector<String> retour= new Vector<String>();
      try (Statement statement = mConnection.createStatement()) {
         ResultSet result = statement.executeQuery("SELECT Nom FROM Festival;");
         while(result.next())
         {
            retour.add(new String (result.getString("Nom")));
         }
      }
      catch (SQLException e) {
          e.printStackTrace();
      }
   
      return retour.toArray();
   }
   
   //Récupèrer une String représentant chaque membre du comité

   public Object[][] getComite(String nomFestival) {
      Object [][] retour;
	      try (Statement statement = mConnection.createStatement()) {
	          ResultSet result = statement.executeQuery(
	        		  							"SELECT Personne.No_personne,"
	        		  						+ 	"		Personne.Nom, "
	          								+	"		Personne.Prenom,"
                                                                                +       "               Personne.No_telephone, "   
                                                                                +       "               Personne.email, "     
	          								+ 	"		Comite.Secteur_responsable "
	          								+ 	"FROM  Comite "
	          								+ 	"	INNER JOIN Personne "
	          								+ 	"	ON Comite.No_Personne = Personne.No_Personne "
	          								+ 	"	INNER JOIN emploie "
	          								+ 	"	ON Personne.No_Personne = emploie.No_Personne "
	          								+ 	"WHERE emploie.Nom_festival = '"+ nomFestival + "'");
                  result.last();
                  retour = new Object[result.getRow()][6];
                  result.beforeFirst();
                  int i = 0;
	          while(result.next())
	          {
                     Object tmp[]={result.getInt(1),result.getString("Nom"),result.getString("Prenom"),result.getString("No_telephone"),result.getString("email"),result.getString("Secteur_responsable")};
                     retour[i]=tmp;
                     i++;
	          }
                  
                  return retour;
	       }
	       catch (SQLException e) {
	           e.printStackTrace();
	       }
	   
	   return null;

   }
   
   //Récupèrer une String représentant chaque membre du staff
   public Object[][] getStaff(String nomFestival) {
	    Object [][] retour;
	      try (Statement statement = mConnection.createStatement()) {
	          ResultSet result = statement.executeQuery(
	        		  							"SELECT Personne.No_personne,"
	        		  						+ 	"		Personne.Nom, "
	          								+	"		Personne.Prenom,"
                                                                                +       "               Personne.No_telephone, "   
                                                                                +       "               Personne.email, "
	          								+ 	"		Staff.Secteur_preferentiel "
	          								+ 	"FROM  Staff "
	          								+ 	"	INNER JOIN Personne "
	          								+ 	"	ON Staff.No_Personne = Personne.No_Personne "
	          								+ 	"	INNER JOIN emploie "
	          								+ 	"	ON Personne.No_Personne = emploie.No_Personne "
	          								+ 	"WHERE emploie.Nom_festival = '"+ nomFestival + "'");
		      
                  result.last();
                  retour = new Object[result.getRow()][6];
                  result.beforeFirst();
                  int i = 0;
	          while(result.next())
	          {	
                     Object tmp[] ={ result.getInt("No_personne") ,result.getString("Nom") , result.getString("Prenom"),result.getString("No_telephone"),result.getString("email") , result.getString("Secteur_preferentiel")};
	             retour[i]=tmp;
                     i++;
	          }
                  return retour;
	       }
	       catch (SQLException e) {
	           e.printStackTrace();
	       }
	   return null;

   }
 //Récupèrer une String représentant chaque scène du festival
   public Object[][] getScene(String nomFestival) {
	   Object retour[][];
           int i = 0;
	      try (Statement statement = mConnection.createStatement()) {
	          ResultSet result = statement.executeQuery(
	        		  							" SELECT Scene.Nom, "
	        		  						+ 	"        Scene.Localisation, Horaire_debut, Horaire_fin, Date_montage, Date_demontage  "	          					
	          								+ 	" FROM  Scene "
	          								+ 	"	INNER JOIN fest_contient_scene "
	          								+ 	"	ON Scene.Nom = fest_contient_scene.Nom_scene "
	          								+ 	" WHERE fest_contient_scene.Nom_festival = '"+ nomFestival + "'");
		      
                  result.last();
                  retour = new Object[result.getRow()][6];
                  result.beforeFirst();
	          while(result.next())
                  {
	        	Object tmp[] = {result.getString("Nom") , result.getString("Localisation"), result.getTimestamp("Horaire_debut"), result.getTimestamp("Horaire_fin"), result.getTimestamp("Date_montage"), result.getTimestamp("Date_Demontage")};
	             retour[i] = tmp;
                     i++;
	          }
                  
                  return retour;
	       }
	       catch (SQLException e) {
	           e.printStackTrace();
	       }
	   return null;

   }
 //Récupèrer une String représentant chaque artiste d'une scène
   public Object[][] getArtiste(String nomScene) 
   {
   Object retour[][];
   int i = 0;
     try (Statement statement = mConnection.createStatement()) {
         ResultSet result = statement.executeQuery(
                                                                           " SELECT Artiste.Nom , Cachet, Contrat, Demandes_spécifiques, Fiche_Technique, Loge, Heure_debut, Heure_fin, Description "	          					
                                                                       +   " FROM  Artiste "
                                                                       +   " WHERE Artiste.Scene ='"+ nomScene + "';");
         result.last();
         retour = new Object[result.getRow()][9];
         result.beforeFirst();
         while(result.next())
         {
            Object[] tmp = {result.getString("Nom"), result.getString("Cachet"), result.getString("Demandes_spécifiques"), result.getString("Fiche_Technique"), result.getString("Loge"),result.getTimestamp("Heure_debut"),result.getTimestamp("Heure_fin"), result.getString("Description")};
            retour[i]=tmp;
            i++;
         }
         
         return retour;
      }
      catch (SQLException e) {
          e.printStackTrace();
      }
  return null;
  }
   //Récupèrer une String représentant chaque stand du festival
    public Object[][] getStand(String nomFestival) 
    {
    Object retour[][];
    int i = 0;
      try (Statement statement = mConnection.createStatement()) {
          ResultSet result = statement.executeQuery(
                                                                            " SELECT Stand.No_Stand, Nom , Localisation, Date_Montage, Date_Demontage "	          					
                                                                        + 	" FROM  Stand "
                                                                        + 	"	INNER JOIN fest_acceuille_stand "
                                                                        + 	"	ON Stand.No_Stand = fest_acceuille_stand.No_stand "
                                                                        + 	" WHERE fest_acceuille_stand.Nom_festival = '"+ nomFestival + "'");
          result.last();
          retour = new Object[result.getRow()][5];
          result.beforeFirst();
          while(result.next())
          {	
             Object[] tmp = {result.getString("No_Stand"),result.getString("Nom"), result.getString("Localisation"),result.getTimestamp("Date_Montage"),result.getTimestamp("Date_demontage")};
             retour[i]=tmp;
             i++;
          }
          
          return retour;
       }
       catch (SQLException e) {
           e.printStackTrace();
       }
   return null;
   }
    
   public Object[][] getStand(String nomFestival, int noStand) 
   {
     Object retour[][];
    int i = 0;
    int nbColTotal=5;
      try (Statement statementA = mConnection.createStatement()) {
         Statement statementB = mConnection.createStatement();
         Statement statementC = mConnection.createStatement();
                         
          ResultSet resultA = statementA.executeQuery(
                                                                            " SELECT Stand.No_Stand, Nom , Localisation, Date_Montage, Date_Demontage "	          					
                                                                        + 	" FROM  Stand "
                                                                        + 	" WHERE No_Stand=" + noStand); 
          ResultSet resultB = statementB.executeQuery (" SELECT Carte_Boisson, Stocks  "
                                                      + " FROM bar WHERE No_Stand=" + noStand);
          ResultSet resultC = statementC.executeQuery ( " SELECT Carte_Nourriture "
                                                      + " FROM Nourriture WHERE No_Stand=" + noStand);
                  
          if (resultB.next())
          {
             nbColTotal+=2;
          }
          if ( resultC.next())
          {
             nbColTotal+=1;
          }
          
          retour = new Object[1][nbColTotal];
          resultA.next();

             Object tmp[];
             if (nbColTotal==5)
                  tmp = new Object[]{resultA.getString("No_Stand"),resultA.getString("Nom"), resultA.getString("Localisation"),resultA.getTimestamp("Date_Montage"),resultA.getTimestamp("Date_demontage")};
             else if (nbColTotal==6)     //Stand nourriture
                  tmp = new Object[]{resultA.getString("No_Stand"),resultA.getString("Nom"), resultA.getString("Localisation"),resultA.getTimestamp("Date_Montage"),resultA.getTimestamp("Date_demontage"),resultC.getString("Carte_Nourriture")};
             else if (nbColTotal==7)   //Stand bar
                  tmp = new Object[]{resultA.getString("No_Stand"),resultA.getString("Nom"), resultA.getString("Localisation"),resultA.getTimestamp("Date_Montage"),resultA.getTimestamp("Date_demontage"),resultB.getString("Carte_Boisson"),resultB.getString("Stocks")};
             else //Stand Bar+ nourriture
                  tmp = new Object[]{resultA.getString("No_Stand"),resultA.getString("Nom"), resultA.getString("Localisation"),resultA.getTimestamp("Date_Montage"),resultA.getTimestamp("Date_demontage"),resultB.getString("Carte_Boisson"),resultB.getString("Stocks"),resultC.getString("Carte_Nourriture")};

             retour[0]=tmp;

          
          return retour;
       }
       catch (SQLException e) {
           e.printStackTrace();
       }
   return null;
   }
    //Récupèrer une String représentant chaque Carte du festival

    public Object[][] getCarte(String nomFestival) 
    {
    Object retour[][];
    int i = 0;
      try (Statement statement = mConnection.createStatement()) {
          ResultSet result = statement.executeQuery(
                                                                                " SELECT Num_carte, Description, Lien "	          					
                                                                        + 	" FROM  Carte "
                                                                        + 	" WHERE Nom_festival = '"+ nomFestival + "'");
          result.last();
          retour = new Object[result.getRow()][3];
          result.beforeFirst();
          while(result.next())
          {	
             Object tmp[] = {result.getInt("Num_carte"),result.getString("Description"),result.getString("Lien")};
             retour[i]=tmp;
             i++;
          }
          return retour;
       }
       catch (SQLException e) {
           e.printStackTrace();
       }
   return null;
    }
    //Récupèrer une String représentant chaque billeterie du festival

    public Object[][] getBilleterie(String nomFestival) 
    {
    Object retour[][];
    int i = 0;
      try (Statement statement = mConnection.createStatement()) {
          ResultSet result = statement.executeQuery(
                                                                                " SELECT Type_de_billet, Tarif "	          					
                                                                        + 	" FROM  billeterie "
                                                                        + 	" WHERE Nom_festival = '"+ nomFestival + "'");
          result.last();
          retour = new Object[result.getRow()][2];
          result.beforeFirst();
          while(result.next())
          {	
             Object tmp[] = {result.getString("Type_de_billet"), result.getDouble("Tarif")};
             retour[i]=tmp;
             i++;
          }
          return retour;
       }
       catch (SQLException e) {
           e.printStackTrace();
       }
    return null;    
    }
    
    //Récupèrer une String représentant chaque materiel du festival
    public Object[][] getMateriel(String nomFestival) 
    {
    Object retour[][];
    int i = 0;
      try (Statement statement = mConnection.createStatement()) {
          ResultSet result = statement.executeQuery(
        		  "SELECT DISTINCT T.No_Materiel, T.Type_Materiel, T.Quantité, T.Prix "
       + 		  "FROM(	SELECT Materiel.No_Materiel, Type_Materiel, Materiel.Quantité, Materiel.Prix "
       + 		  		"FROM Materiel "
       + 		  		"INNER JOIN scene_besoin_mat "
        +		  		"ON scene_besoin_mat.No_Materiel = Materiel.No_Materiel "
        +		  		"INNER JOIN fest_contient_scene "
        +		  		"ON (fest_contient_scene.Nom_scene = scene_besoin_mat.Nom_scene AND fest_contient_scene.Nom_festival = '" + nomFestival + "') "
        +		  	"UNION "
        +		  		"SELECT Materiel.No_Materiel, Type_Materiel, Materiel.Quantité, Materiel.Prix "
        +		  		"FROM Materiel "
        +		  		"INNER JOIN  fest_besoin_mat "
        +		  		"ON (fest_besoin_mat.No_Materiel = Materiel.No_Materiel AND fest_besoin_mat.Nom_festival = '" + nomFestival + "') "
        +		  	"UNION "
        +		  		"SELECT Materiel.No_Materiel, Type_Materiel, Materiel.Quantité, Materiel.Prix "
        +		  		"FROM Materiel "
        +		        "INNER JOIN stand_besoin_mat "
        +		  		"ON stand_besoin_mat.No_Materiel = Materiel.No_Materiel "
        +		  		"INNER JOIN fest_acceuille_stand "
        +		  		"ON (fest_acceuille_stand.No_stand = stand_besoin_mat.No_stand AND fest_acceuille_stand.Nom_festival = '"+ nomFestival +"') "
        +		  	") AS T");
          
          result.last();
          retour = new Object[result.getRow()][4];
          result.beforeFirst();
          while(result.next())
          {	
            Object tmp[] = {result.getInt("No_Materiel"), result.getString("Type_Materiel"),result.getInt("Quantité"),result.getDouble("Prix")};
             retour[i]=tmp;
             i++;
          }
          return retour;
       }
       catch (SQLException e) {
           e.printStackTrace();
       }
   return null;
    }
    
    // FONCTIONS D'INSERTION
    //Inserer un festival dans la base de donnée
    public void insertFestival(String nomFestival, String Date_debut, String Date_fin, Double Budget_Global){
 	   Statement statement = null;
      try {
     	 mConnection.setAutoCommit(false);
     	 statement = mConnection.createStatement();
     	 String insertFestival = 		" INSERT INTO Festival (Nom, Date_debut, Date_fin, Budget_Global)" 
     			 					+ 	" VALUES ('" + nomFestival + "' , STR_TO_DATE('" + Date_debut + "', '%Y/%m/%e %T') , STR_TO_DATE('" + Date_fin + "', '%Y/%m/%e %T') ," + Budget_Global + ");";
     	 statement.executeUpdate(insertFestival);
     	 mConnection.commit();

      }
      catch (SQLException e) {
     	 e.printStackTrace();
      }	    
    }
   //Inserer un comité dans la base de donnée
   public void insertComité(String nomFestival, String nom, String prenom, String noTel, String email, String sectResp){
	   Statement statement = null;
     try {
    	 mConnection.setAutoCommit(false);
    	 statement = mConnection.createStatement();
    	 String insertPersonne = 		" INSERT INTO Personne (Nom, Prenom, No_telephone, Email)" 
    			 					+ 	" VALUES ('" + nom + "' , '" + prenom + "' , '" + noTel + "' , '" + email + "');";
    	 statement.executeUpdate(insertPersonne);
    	 
    	 String insertEmploie = 		" INSERT INTO emploie (Nom_festival, No_personne)" 
									+ 	" VALUES ('" + nomFestival + "' , LAST_INSERT_ID());";
    	 statement.executeUpdate(insertEmploie);
    	 
    	 String insertComite = 			" INSERT INTO Comite (No_personne, Secteur_responsable)" 
									+ 	" VALUES (LAST_INSERT_ID() , '" + sectResp + "');";
    	 statement.executeUpdate(insertComite);
    	 mConnection.commit();

     }
     catch (SQLException e) {
    	 e.printStackTrace();
     }	    
   }
   //Inserer un staff dans la base de donnée
   public void insertStaff(String nomFestival, String nom,  String prenom, String noTel, String email, String sectPref){
	   Statement statement = null;
     try {
    	 mConnection.setAutoCommit(false);
    	 
    	 statement = mConnection.createStatement();
    	 String insertPersonne = 		" INSERT INTO Personne (Nom, Prenom, No_telephone, Email)" 
    			 					+ 	" VALUES ('" + nom + "' , '" + prenom + "' , '" + noTel + "' , '" + email + "');";
    	 statement.executeUpdate(insertPersonne);
    	 
    	 String insertEmploie = 		" INSERT INTO emploie (Nom_festival, No_personne)" 
					+ 	" VALUES ('" + nomFestival + "' , LAST_INSERT_ID());";
    	 statement.executeUpdate(insertEmploie);

    	 String insertStaff = 			" INSERT INTO Staff (No_personne, Secteur_preferentiel)" 
									+ 	" VALUES (LAST_INSERT_ID() , '" + sectPref + "');";
    	 statement.executeUpdate(insertStaff);
    	 
    	 mConnection.commit();
     }
     catch (SQLException e) {
    	 e.printStackTrace();
     }	    
   }
   //Inserer un stand dans la base de donnée
   //Insertion d'un stand Bar
   public void insertStandBar(String nomFestival, String nom, String Localisation, String Date_Montage, String Date_Demontage, String Contact_Fournisseur,  String Carte_Boisson, String Stocks){
	   Statement statement = null;
     try {
    	 mConnection.setAutoCommit(false);
    	 
    	 statement = mConnection.createStatement();
    	 String insertStand = 		" INSERT INTO Stand (Nom, Localisation, Date_Montage, Date_Demontage, Contact_Fournisseur)" 
    			 					+ 	" VALUES ('" + nom + "' , '" + Localisation + "' , " + "STR_TO_DATE('" + Date_Montage + "', '%Y/%m/%e %T') , " + "STR_TO_DATE('" + Date_Demontage + "', '%Y/%m/%e %T') , " 
    			 					+ 	" '" + Contact_Fournisseur + "');";
    	 statement.executeUpdate(insertStand);
    	 
    	 String insertBar = 	" INSERT INTO Bar (No_Stand, Carte_Boisson, Stocks)" 
								+ 	"VALUES (LAST_INSERT_ID() , '" + Carte_Boisson + "' , '" + Stocks + "');";
    	 statement.executeUpdate(insertBar);

    	 String insertFestAcceuilleStand = 			" INSERT INTO fest_acceuille_stand (Nom_festival, No_stand)" 
												+ 	" VALUES ('" + nomFestival + "', + LAST_INSERT_ID());";
    	 statement.executeUpdate(insertFestAcceuilleStand);
    	 
    	 mConnection.commit();
     }
     catch (SQLException e) {
    	 e.printStackTrace();
     }	    
   }
   //Insertion d'un stand Nourriture
   public void insertStandNourriture(String nomFestival, String nom, String Localisation, String Date_Montage, String Date_Demontage, String Contact_Fournisseur,  String Carte_Nourriture){
	   Statement statement = null;
     try {
    	 mConnection.setAutoCommit(false);
    	 
    	 statement = mConnection.createStatement();
    	 String insertStand = 		" INSERT INTO Stand (Nom, Localisation, Date_Montage, Date_Demontage, Contact_Fournisseur)" 
    			 					+ 	" VALUES ('" + nom + "' , '" + Localisation + "' , " + "STR_TO_DATE('" + Date_Montage + "', '%Y/%m/%e %T') , " + "STR_TO_DATE('" + Date_Demontage + "', '%Y/%m/%e %T') , " 
    			 					+ 	" '" + Contact_Fournisseur + "');";
    	 statement.executeUpdate(insertStand);
    	 
    	 String insertNourriture = 	" INSERT INTO Nourriture (No_Stand, Carte_Nourriture)" 
								+ 	"VALUES (LAST_INSERT_ID() , '" + Carte_Nourriture + "');";
    	 statement.executeUpdate(insertNourriture);

    	 String insertFestAcceuilleStand = 			" INSERT INTO fest_acceuille_stand (Nom_festival, No_stand)" 
												+ 	" VALUES ('" + nomFestival + "', + LAST_INSERT_ID());";
    	 statement.executeUpdate(insertFestAcceuilleStand);
    	 
    	 mConnection.commit();
     }
     catch (SQLException e) {
    	 e.printStackTrace();
     }	    
   }
   //Insertion d'un stand Bar et Nourriture
   public void insertStandBarNourriture(String nomFestival, String nom, String Localisation, String Date_Montage, String Date_Demontage, String Contact_Fournisseur,  String Carte_Nourriture, String Carte_Boisson, String Stocks){
	   Statement statement = null;
     try {
    	 mConnection.setAutoCommit(false);
    	 
    	 statement = mConnection.createStatement();
    	 String insertStand = 		" INSERT INTO Stand (Nom, Localisation, Date_Montage, Date_Demontage, Contact_Fournisseur)" 
    			 					+ 	" VALUES ('" + nom + "' , '" + Localisation + "' , " + "STR_TO_DATE('" + Date_Montage + "', '%Y/%m/%e %T') , " + "STR_TO_DATE('" + Date_Demontage + "', '%Y/%m/%e %T') , " 
    			 					+ 	" '" + Contact_Fournisseur + "');";
    	 statement.executeUpdate(insertStand);
    	 
    	 String insertNourriture = 	" INSERT INTO Nourriture (No_Stand, Carte_Nourriture)" 
								+ 	"VALUES (LAST_INSERT_ID() , '" + Carte_Nourriture + "');";
    	 statement.executeUpdate(insertNourriture);
    	 
    	 String insertBar = 		" INSERT INTO Bar (No_Stand, Carte_Boisson, Stocks)" 
								+ 	"VALUES (LAST_INSERT_ID() , '" + Carte_Boisson + "' , '" + Stocks + "');";
statement.executeUpdate(insertBar);

    	 String insertFestAcceuilleStand = 			" INSERT INTO fest_acceuille_stand (Nom_festival, No_stand)" 
												+ 	" VALUES ('" + nomFestival + "', + LAST_INSERT_ID());";
    	 statement.executeUpdate(insertFestAcceuilleStand);
    	 
    	 mConnection.commit();
     }
     catch (SQLException e) {
    	 e.printStackTrace();
     }	    
   }
   
   //Inserer une scene dans la base de donnée
   public void insertScene(String nomFestival, String nom, String localisation, String Horaire_debut, String Horaire_fin, String Date_Montage, String Date_Demontage){
	   Statement statement = null;
     try {
    	 mConnection.setAutoCommit(false);
    	 statement = mConnection.createStatement();
    	 String insertScene = 		" INSERT INTO Scene (Nom, Localisation, Horaire_debut, Horaire_fin, Date_Montage, Date_Demontage)" 
    			 					+ 	" VALUES ('" + nom + "' , '" + localisation + "' , " +  "STR_TO_DATE('" + Horaire_debut + "', '%Y/%m/%e %T') , " 
    			 					+   "STR_TO_DATE('" + Horaire_fin + "', '%Y/%m/%e %T') , " +  "STR_TO_DATE('" + Date_Montage + "', '%Y/%m/%e %T') , " 
    			 					+	"STR_TO_DATE('" + Date_Demontage + "', '%Y/%m/%e %T'));";
    	 statement.executeUpdate(insertScene);
    	 
    	 String insertEmploie = 		" INSERT INTO fest_contient_scene (Nom_festival, Nom_scene)" 
					+ 	" VALUES ('" + nomFestival + "' , '" + nom + "');";
    	 statement.executeUpdate(insertEmploie);
    	 mConnection.commit();

     }
     catch (SQLException e) {
    	 e.printStackTrace();
     }	    
   }
    //Inserer un artiste dans la base de donnée
   public void insertArtiste(String nomScene, String nom, double Cachet, String Contrat, String Demandes_spécifiques, String Fiche_Technique, String Loge, String Heure_debut, String Heure_fin, String Description){
	   Statement statement = null;
     try {
    	 mConnection.setAutoCommit(false);
    	 statement = mConnection.createStatement();
    	 String insertArtiste = 		" INSERT INTO Artiste (Nom, Cachet, Contrat, Demandes_spécifiques, Fiche_Technique, Loge, Scene, Heure_debut, Heure_fin, Description)" 
    			 					+ 	" VALUES ('" + nom + "' , '" + Cachet + "' , '" + Contrat + "' , '" + Demandes_spécifiques + "' , '" + Fiche_Technique + "' , '" + Loge + "' , '" + nomScene + "' , " 
    			 					+  	"STR_TO_DATE('" + Heure_debut + "', '%Y/%m/%e %T') , " + "STR_TO_DATE('" + Heure_fin + "', '%Y/%m/%e %T') , '" + Description + "');";
    	 statement.executeUpdate(insertArtiste);
    	 
    	 mConnection.commit();

     }
     catch (SQLException e) {
    	 e.printStackTrace();
     }	    
   }
   //Inserer une carte dans la base de donnée
   public void insertCarte(String nomFestival, String Description, String Lien){
	   Statement statement = null;
     try {
    	 mConnection.setAutoCommit(false);
    	 statement = mConnection.createStatement();
    	 String insertCarte = 		" INSERT INTO Carte (Description, Lien, Nom_Festival)" 
    			 					+ 	" VALUES ('" + Description + "' , '" + Lien + "' , '" +  nomFestival + "');";
    	 statement.executeUpdate(insertCarte);
    	 mConnection.commit();

     }
     catch (SQLException e) {
    	 e.printStackTrace();
     }	    
   }
   //Inserer une billeterie dans la base de donnée
   public void insertBilleterie(String nomFestival, String Type_de_billet, Double Tarif){
	   Statement statement = null;
     try {
    	 mConnection.setAutoCommit(false);
    	 statement = mConnection.createStatement();
    	 String insertCarte = 		" INSERT INTO Billeterie (Type_de_billet, Tarif, Nom_Festival)" 
    			 					+ 	" VALUES ('" + Type_de_billet + "' , " + Tarif + " , '" +  nomFestival + "');";
    	 statement.executeUpdate(insertCarte);
    	 mConnection.commit();

     }
     catch (SQLException e) {
    	 e.printStackTrace();
     }	    
   }
  //Inserer du matériel pour le festival dans la base de donnée
   public void insertMaterielFest(String nomFestival, String Type_Materiel, String Description, Integer Quantité, String Contact_responsable, Double Prix){
	   Statement statement = null;
     try {
    	 mConnection.setAutoCommit(false);
    	 statement = mConnection.createStatement();
    	 String insertMat = 		" INSERT INTO Materiel (Type_Materiel, Quantité, Contact_responsable, Prix)" 
    			 					+ 	" VALUES ('" + Type_Materiel + "' , " + Quantité + " , '" +  Contact_responsable + "' , " + Prix + ");";
    	 statement.executeUpdate(insertMat);
    	 
    	 String insertMatFest = 		" INSERT INTO fest_besoin_mat (Nom_festival, No_materiel, Description, Quantité, Prix)" 
					+ 	" VALUES ('" + nomFestival + "' , LAST_INSERT_ID() , '" + Description + "' , " + Quantité + " , " + Prix + " );";
    	 statement.executeUpdate(insertMatFest);
    	 mConnection.commit();

     }
     catch (SQLException e) {
    	 e.printStackTrace();
     }	    
   }

   //Inserer du matériel pour un stand dans la base de donnée
   public void insertMaterielStand( Integer numeroStand, String Type_Materiel, String Description, Integer Quantité, String Contact_responsable, Double Prix){
	   Statement statement = null;
     try {
    	 mConnection.setAutoCommit(false);

    	 statement = mConnection.createStatement();
    	 String insertMat = 		" INSERT INTO Materiel (Type_Materiel, Quantité, Contact_responsable, Prix)" 
    			 					+ 	" VALUES ('" + Type_Materiel + "' , " + Quantité + " , '" +  Contact_responsable + "' , " + Prix + ");";
    	 statement.executeUpdate(insertMat);
    	 
    	 String insertMatStand = 		" INSERT INTO stand_besoin_mat (No_stand, No_materiel, Description, Quantité, Prix)" 
					+ 	" VALUES ( " + numeroStand + ", LAST_INSERT_ID() , '" + Description + "' , " + Quantité + " , " + Prix + " );";
    	 statement.executeUpdate(insertMatStand);
    	 mConnection.commit();

     }
     catch (SQLException e) {
    	 e.printStackTrace();
     }	    
   }

   //Inserer du matériel pour une scène dans la base de donnée

   public void insertMaterielScene(String nomScene, String Type_Materiel, String Description, Integer Quantité, String Contact_responsable, Double Prix){
	   Statement statement = null;
     try {
    	 mConnection.setAutoCommit(false);

    	 statement = mConnection.createStatement();
    	 String insertMat = 		" INSERT INTO Materiel (Type_Materiel, Quantité, Contact_responsable, Prix)" 
    			 					+ 	" VALUES ('" + Type_Materiel + "' , " + Quantité + " , '" +  Contact_responsable + "' , " + Prix + ");";
    	 statement.executeUpdate(insertMat);
    	 
    	 String insertMatStand = 		" INSERT INTO scene_besoin_mat (Nom_scene, No_materiel, Description, Quantité, Prix)" 
					+ 	" VALUES ( '" + nomScene + "', LAST_INSERT_ID() , '" + Description + "' , " + Quantité + " , " + Prix + " );";
    	 statement.executeUpdate(insertMatStand);
    	 mConnection.commit();

     }
     catch (SQLException e) {
    	 e.printStackTrace();
     }	    
   }

   
}

